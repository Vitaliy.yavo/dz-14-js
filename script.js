const themeButton = document.querySelector(".theme-button");
const elementsToChange = document.querySelectorAll(".changeable");

function toggleTheme() {
  const currentTheme = localStorage.getItem("theme");
  const newTheme = currentTheme === "dark" ? "light" : "dark";
  localStorage.setItem("theme", newTheme);
  elementsToChange.forEach((element) => {
    element.classList.toggle("dark-theme", newTheme === "dark");
  });
}

themeButton.addEventListener("click", toggleTheme);

function initializeTheme() {
  const currentTheme = localStorage.getItem("theme");
  if (currentTheme === "dark") {
    elementsToChange.forEach((element) => {
      element.classList.add("dark-theme");
    });
  }
}
initializeTheme();
